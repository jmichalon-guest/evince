commit 8b2f2075a9d9f0e5a9f389c758bbc44dfa71b2b6
Author: Samuel Thibault <samuel.thibault@ens-lyon.org>
Date:   Fri Aug 30 15:25:46 2019 +0200

    ev-view-accessible: Initialize private fields
    
    Otherwise the default zero initialization will make
    ev_view_accessible_set_page_range think that we already have page range
    0-0 set.

---
 libview/ev-view-accessible.c |   15 ++++++++++++---
 1 file changed, 12 insertions(+), 3 deletions(-)

--- a/libview/ev-view-accessible.c
+++ b/libview/ev-view-accessible.c
@@ -121,6 +121,8 @@ static void
 ev_view_accessible_initialize (AtkObject *obj,
 			       gpointer   data)
 {
+	EvViewAccessiblePrivate *priv;
+
 	if (ATK_OBJECT_CLASS (ev_view_accessible_parent_class)->initialize != NULL)
 		ATK_OBJECT_CLASS (ev_view_accessible_parent_class)->initialize (obj, data);
 
@@ -128,6 +130,11 @@ ev_view_accessible_initialize (AtkObject
 
 	atk_object_set_name (obj, _("Document View"));
 	atk_object_set_role (obj, ATK_ROLE_DOCUMENT_FRAME);
+
+	priv = EV_VIEW_ACCESSIBLE (obj)->priv;
+	priv->previous_cursor_page = -1;
+	priv->start_page = 0;
+	priv->end_page = -1;
 }
 
 gint
@@ -330,9 +337,11 @@ ev_view_accessible_cursor_moved (EvView
 		AtkObject *previous_page = NULL;
 		AtkObject *current_page = NULL;
 
-		previous_page = g_ptr_array_index (priv->children,
-						   priv->previous_cursor_page);
-		atk_object_notify_state_change (previous_page, ATK_STATE_FOCUSED, FALSE);
+		if (priv->previous_cursor_page >= 0) {
+			previous_page = g_ptr_array_index (priv->children,
+							   priv->previous_cursor_page);
+			atk_object_notify_state_change (previous_page, ATK_STATE_FOCUSED, FALSE);
+		}
 		priv->previous_cursor_page = page;
 		current_page = g_ptr_array_index (priv->children, page);
 		atk_object_notify_state_change (current_page, ATK_STATE_FOCUSED, TRUE);
